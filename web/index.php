<?php

// putenv('DATASTORE_EMULATOR_HOST = http://localhost:8081');

require_once __DIR__.'/../vendor/autoload.php';

/** @var  Silex\Application\ $app */
$app = require __DIR__.'/../src/app.php';

require __DIR__.'/../src/controllers.php';
require __DIR__.'/../src/DataModel/AbstractModel.php';
require __DIR__.'/../src/DataModel/UserModel.php';
require __DIR__.'/../src/DataModel/BoardModel.php';
require __DIR__.'/../src/DataModel/ListModel.php';
require __DIR__.'/../src/DataModel/CardModel.php';


$app->run();