<?php

use Silex\Application;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use SimpleTrello\DataModel\BoardModel;
use SimpleTrello\DataModel\CardModel;
use SimpleTrello\DataModel\ListModel;
use SimpleTrello\DataModel\UserModel;

$app = new Application();

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../templates',
    'twig.options' => array(
            'strict_variables' => false,
        ),
));

$app->register(new SessionServiceProvider());

$app['debug'] = true;

$app['model.user'] = function ($app) {
  return new UserModel("projettest1");
};

$app['model.board'] = function ($app) {
    return new BoardModel("projettest1");
};

$app['model.list'] = function ($app) {
    return new ListModel("projettest1");
};

$app['model.card'] = function ($app) {
    return new CardModel("projettest1");
};

return $app;