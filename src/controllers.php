<?php

use SimpleTrello\DataModel\UserModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

$app->get('/', function(Request $request) use($app) {
    $twig = $app['twig'];
    $LoggedUser = $app['session']->get('user')["email"];
    return $twig->render('index.html.twig', array(
        'action'=>'index',
        'LoggedUser'=>$LoggedUser
    ));
});

$app->get('/login', function(Request $request) use($app) {
    return $app['twig']->render('login.html.twig', array());
});

$app->get('/logout', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $app['session']->set('user', null);
    return $app->redirect("./");
});

$app->get('/users', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $LoggedUser = $app['session']->get('user')["email"];
    $allUsers =  $app['model.user']->listAll();
    return $app['twig']->render('users.html.twig', array('users'=>$allUsers,
        'LoggedUser'=>$LoggedUser));
});

$app->get('/deleteUser', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $id = $request->get("id");
    $app['model.user']->delete($id);
    return $app->redirect("users");
});

$app->get('/createUser', function(Request $request) use($app) {

    $email = $request->get("email");
    $password = $request->get("password");
    $objBcrypt = new BCryptPasswordEncoder(5);

    $data = array('email' => $email, 'password'=>$objBcrypt->encodePassword($password, 'iutbayonnesalt'));

    $app['model.user']->create($data);
    return $app->redirect("users");
});

$app->post('/login', function(Request $request) use($app) {
    $TypedPassword = $request->get("password");
    $typedEmail = $request->get("email");

    /** @var \SimpleTrello\DataModel\UserModel $userModel */
    $found = $app['model.user']->findByEmail($typedEmail);
    if($found == null) die("Invalid Email");
    $correctPassword = reset($found)['password'];
    $objBcrypt = new BCryptPasswordEncoder(5);
    $succes = $objBcrypt->isPasswordValid($correctPassword, $TypedPassword, "iutbayonnesalt");

    if ($succes) {
        $data = array(
            "email"    => $typedEmail,
            "password" => $TypedPassword,
        );

        $app['session']->set('user', $data);

        return $app->redirect('/');
    } else {
        die("Invalid Password");
    }
});

$app->get('/createDefaultUser', function (Request $request) use ($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }
    $objBcrypt = new BCryptPasswordEncoder(5);

    $datas = array(
        'email'    => 'ribes.david@gmail.com',
        'password' => $objBcrypt->encodePassword('password', 'iutbayonnesalt'),
    );

    $app['model.user']->create($datas);

    return $app->redirect('login');
});

$app->get('/register', function(Request $request) use($app) {
    return $app['twig']->render('register.html.twig', array());
});

$app->get('/boards', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }
    $twig = $app['twig'];
    $LoggedUser = $app['session']->get('user')["email"];

    $allBoards =  $app['model.board']->listAll();

    return $twig->render('boards.html.twig', array(
        'LoggedUser'=>$LoggedUser,
        'boards' => $allBoards
    ));
});

$app->get('/addBoard', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $name = $request->get("name");

    $user = $app['session']->get('user');

    $userdata = $app['model.user']->findBy('email', $user['email']);

    $id = key($userdata);

    $data = array(
        'titre' => $name,
        'userid'=> $id
    );

    $app['model.board']->create($data);

    $boardCreated = $app['model.board']->findBy('titre',$name);

    $boardid = key($boardCreated);

    $todo = array(
        'titre' => "To Do",
        'boardid'=> $boardid
    );

    $app['model.list']->create($todo);

    $doing = array(
        'titre' => "Doing",
        'boardid'=> $boardid
    );

    $app['model.list']->create($doing);

    $done = array(
        'titre' => "Done",
        'boardid'=> $boardid
    );

    $app['model.list']->create($done);

    return $app->redirect("boards");

});

$app->get('/deleteBoard', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }
    $id = $request->get("id");
    $app['model.board']->delete($id);

    return $app->redirect("boards");
});

$app->get('/viewBoard', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $id = $request->get("id");

    $LoggedUser = $app['session']->get('user')["email"];

    $allist =   $app['model.list']->listAll();
    $allcards=   $app['model.card']->listAll();

    return $app['twig']->render('list.html.twig', array(
        'LoggedUser'=> $LoggedUser,
        'boardid'=> $id,
        'alllists'=> $allist,
        'allcards'=> $allcards
    ));

});

$app->get('/addList', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $name = $request->get("name");
    $id = $request->get("boardid");

    $data = array(
        'titre' => $name,
        'boardid'=> $id
    );

    $app['model.list']->create($data);

    $currentlist = $app['model.list']->findBy('titre', $name);

   $bid = reset($currentlist)['boardid'];

    return $app->redirect("viewBoard?id=".$bid);
});

$app->get('/deleteList', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $id = $request->get("id");

    $currentlist = $app['model.list']->read($id);

    $app['model.list']->delete($id);

    return $app->redirect("viewBoard?id=". $currentlist['boardid']);
});

$app->get('/deleteCard', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $id = $request->get("id");

    $currentcardlistid = ($app['model.card']->read($id))['listid'];

    $currentboardid = ($app['model.list']->read($currentcardlistid))['boardid'];

    $app['model.card']->delete($id);

    return $app->redirect("viewBoard?id=".$currentboardid);
});

$app->get('/addCard', function(Request $request) use($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('login');
    }

    $name = $request->get("name");
    $desc = $request->get("desc");
    $id = $request->get("id");

    $currentlist = $app['model.list']->read($id);

    $data = array(
        'titre' => $name,
        'description'=> $desc,
        'listid'=> $id
    );

    $app['model.card']->create($data);

    return $app->redirect("viewBoard?id=".$currentlist['boardid']);
});

