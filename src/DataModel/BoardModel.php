<?php

namespace SimpleTrello\DataModel;

use Google\Cloud\ServiceBuilder;

class BoardModel extends AbstractModel
{
    protected $columns = [
        'id'=>'integer',
        'titre'=>'string',
        'userid'=>'integer'
    ];

    /**
     * UserModel constructor.
     */
    public function __construct($projectId)
    {
        $this->kind = "Board";
        $this->datasetId = $projectId;
        $cloud = new ServiceBuilder(
            [
                'keyFilePath' => __DIR__.'/../../config/apiKey.json',
                'projectId' => $projectId
            ]
        );

        $this->datastore = $cloud->datastore();
    }
}