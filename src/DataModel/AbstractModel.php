<?php

namespace SimpleTrello\DataModel;

abstract class AbstractModel
{
    protected $datasetId;
    protected $datastore;
    protected $kind;

    public function read($id)
    {
        $key = $this->datastore->key($this->kind, $id);
        $entity = $this->datastore->lookup($key);

        if ($entity) {
            $e = $entity->get();
            $e['id'] = $id;
            return $e;
        }

        return false;
    }

    public function delete($id)
    {
        $key = $this->datastore->key($this->kind, $id);
        return $this->datastore->delete($key);
    }

    public function findBy($property, $value)
    {
        $query = $this->datastore->query()
            ->kind($this->kind)
            ->filter($property,'=',$value);

        $results = $this->datastore->runQuery($query);

        $entities = [];
        foreach ($results as $entity) {
            $e = $entity->get();
            $entities[$entity->key()->pathEndIdentifier()] = $e;
        }

        return $entities;
    }

    public function listAll()
    {
        $query = $this->datastore->query()
            ->kind($this->kind);

        $results = $this->datastore->runQuery($query);

        $objs = [];
        foreach ($results as $entity) {
            $obj = $entity->get();
            $objs[$entity->key()->pathEndIdentifier()] = $obj;
        }


        return $objs;
    }


    public function create($obj, $key=null)
    {
        $this->verify($obj);
        $key = $this->datastore->key($this->kind);
        $entity = $this->datastore->entity($key, $obj);

        $this->datastore->insert($entity);

        // return the ID of the created datastore entity
        return $entity->key()->pathEndIdentifier();
    }

    public function update($key, $obj)
    {
        unset($obj['id']);
        $this->verify($obj);
        $transaction = $this->datastore->transaction();
        $key = $this->datastore->key($this->kind, $key);
        $task = $transaction->lookup($key);
        $entity = $this->datastore->entity($key, $obj);
        $transaction->upsert($entity);
        $transaction->commit();

    }

    public function verify($board)
    {
        if ($invalid = array_diff_key($board, $this->columns)) {
            throw new \InvalidArgumentException(sprintf(
                'unsupported user properties: "%s"',
                implode(', ', $invalid)
            ));
        }
    }


    public function __call($method, $args)
    {
        $by = substr($method, 6);
        $method = 'findBy';
        $fieldName = lcfirst($by);
        return $this->$method($fieldName, $args[0]);
    }
}