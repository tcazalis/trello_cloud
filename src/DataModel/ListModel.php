<?php

namespace SimpleTrello\DataModel;

use Google\Cloud\ServiceBuilder;

class ListModel extends AbstractModel
{
    protected $columns = [
        'id'=>'integer',
        'titre'=>'string',
        'boardid'=>'integer'
    ];

    /**
     * UserModel constructor.
     */
    public function __construct($projectId)
    {
        $this->kind = "List";
        $this->datasetId = $projectId;
        $cloud = new ServiceBuilder(
            [
                'keyFilePath' => __DIR__.'/../../config/apiKey.json',
                'projectId' => $projectId
            ]
        );

        $this->datastore = $cloud->datastore();
    }
}