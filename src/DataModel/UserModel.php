<?php

namespace SimpleTrello\DataModel;

use Google\Cloud\ServiceBuilder;

class UserModel extends AbstractModel
{
    protected $columns = [
        'id'=>'integer',
        'email'=>'string',
        'password'=>'string'
    ];

    /**
     * UserModel constructor.
     */
    public function __construct($projectId)
    {
        $this->kind = "User";
        $this->datasetId = $projectId;
        $cloud = new ServiceBuilder(
          [
              'keyFilePath' => __DIR__.'/../../config/apiKey.json',
              'projectId' => $projectId
          ]
        );

        $this->datastore = $cloud->datastore();
    }
}