<?php

namespace SimpleTrello\DataModel;

use Google\Cloud\ServiceBuilder;

class CardModel extends AbstractModel
{
    protected $columns = [
        'id'=>'integer',
        'titre'=>'string',
        'description'=>'string',
        'listid'=>'integer'
    ];

    /**
     * UserModel constructor.
     */
    public function __construct($projectId)
    {
        $this->kind = "Card";
        $this->datasetId = $projectId;
        $cloud = new ServiceBuilder(
            [
                'keyFilePath' => __DIR__.'/../../config/apiKey.json',
                'projectId' => $projectId
            ]
        );

        $this->datastore = $cloud->datastore();
    }
}